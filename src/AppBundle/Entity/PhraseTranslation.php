<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * PhraseTranslation
 *
 * @ORM\Table(name="phrase_translation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PhraseTranslationRepository")
 */
class PhraseTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="phrase", type="string", length=255)
     */
    protected $phrase;


    /**
     * Set phrase
     *
     * @param string $phrase
     *
     * @return PhraseTranslation
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;

        return $this;
    }

    /**
     * Get phrase
     *
     * @return string
     */
    public function getPhrase()
    {
        return $this->phrase;
    }
}

