<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Phrase;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPhrasesData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $phrase1 = new Phrase();
        $phrase2 = new Phrase();
        $phrase3 = new Phrase();
        $phrase4 = new Phrase();

        $phrase1->translate('ru')->setPhrase('Да нет, наверное');
        $phrase1->translate('en')->setPhrase('No, probably');
        $phrase1->translate('de')->setPhrase('No, probablemente');
        $phrase1->translate('es')->setPhrase('Nein, wahrscheinlich');
        $phrase1->translate('pt')->setPhrase('Não, provavelmente');
        $phrase1->translate('fr')->setPhrase('Non, probablement');

        $phrase2->translate('ru')->setPhrase('Не сходи с тропы');
        $phrase2->translate('en')->setPhrase('Do not descend from the trail');
        $phrase2->translate('pt')->setPhrase('Não desça da trilha');
        $phrase2->translate('fr')->setPhrase('Ne descendez pas du sentier');

        $phrase3->translate('ru')->setPhrase('А где мой мишка Тибберс?');
        $phrase3->translate('en')->setPhrase('Where is my teddy bear Tibbs?');
        $phrase3->translate('de')->setPhrase('Wo ist mein Teddybär?');
        $phrase3->translate('es')->setPhrase('¿Dónde está mi oso de peluche?');
        $phrase3->translate('pt')->setPhrase('cadê meu ursinho de pelúcia?');

        $phrase4->translate('ru')->setPhrase('Не подскажете время?');
        $phrase4->translate('en')->setPhrase('Do not tell me the time?');
        $phrase4->translate('de')->setPhrase('Erzähl mir nicht die Zeit?');

        $manager->persist($phrase1);
        $manager->persist($phrase2);
        $manager->persist($phrase3);
        $manager->persist($phrase4);
        $phrase1->mergeNewTranslations();
        $phrase2->mergeNewTranslations();
        $phrase3->mergeNewTranslations();
        $phrase4->mergeNewTranslations();
        $manager->flush();

    }

}
